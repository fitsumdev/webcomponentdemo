
## Web Component Example 
### Steps to run the demo application 
1. Clone the repo from gitlab
2. Go the the project folder and install packages (npm install)
3. Build the application (npm run build)
4. Update the main src in the Demo html with the latest from the build folder path
5. Run the demo html page 

### Note 
There are different  case scenarios that are covered 
1. Token and Uuid validation (check the commented code in the dem html)
2. Token is valid (with mock validation method)
3. Expiring token after two seconds(with mock setup)
