import React, { useState,useEffect } from 'react';
import validateToken from './util/utility'
 
const Counter = ({token,uuid}) => {

  const [count, setCount] = useState(0);
  const [isTokenValid, setTokenValid] = useState(true);

  //validate token on component mount 
  useEffect(() => {
    const isValid = validateToken(token);
    if (!isValid) {
      setTokenValid(false);
    }
  }, []);

  //Invalidate token after on a specific time interval
  setInterval(x=>{
    let isValid = validateToken(token);
    isValid = false; // mock 
    if (!isValid) {
      setTokenValid(false);
    }
  },3000);
  
  
  if(!token)
  {
    return <p>Please Pass token</p>
  }
  if(!uuid)
  {
    return <p>Please Pass dossier uuid</p>
  }

  //check if token is valid 
  if(!isTokenValid)
  {
    return <p>Token is not valid one :)</p>
  }


  const onInc = () => {
    setCount(prev => prev + 1);

  }

  const onDec = () => {
    setCount(prev => prev - 1);

  }

  return(
    <div>
      <h2>Demo Application</h2>
      <h2>Counter: <b>{count}</b></h2>
      <button onClick={onInc}>Increment</button>
      <button onClick={onDec}>Decrement</button>
      <p>----------------------------------------</p>
      <h2>Data Passed</h2>
      <p>Token : {token}</p>
      <p>UUID : {uuid}</p>
      </div>
    )
}


export default Counter