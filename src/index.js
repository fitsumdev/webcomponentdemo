import Counter from './Counter';
import React from 'react';
import ReactDOM from "react-dom";
import r2wc from "react-to-webcomponent";



customElements.define("react-counter", r2wc(Counter, React, ReactDOM, {
    props: {
      token: "string",
      uuid: "string"
    },
  }));

